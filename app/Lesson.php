<?php

namespace App;

use App\Model;

class Lesson extends Model
{
    protected $fillable = ['title', 'body'];

    // protected $hidden = ['id', 'created_at', 'updated_at'];
}
