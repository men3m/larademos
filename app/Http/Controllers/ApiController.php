<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController
{
    /**
     * $statusCode
     *
     * @var integer
     */
    protected $statusCode = 200;

    /**
     * getStatusCode
     *
     * @return void
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * setStatusCode
     *
     * @param mixed $statusCode
     * @return void
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * respondNotFound
     *
     * @param mixed $message
     * @return void
     */
    public function respondNotFound($message = 'Not Found!')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * respondInternalError
     *
     * @param mixed $message
     * @return void
     */
    public function respondInternalError($message = 'Internal Error!')
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }

    /**
     * respond
     *
     * @param mixed $data
     * @param mixed $headers
     * @return void
     */
    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * respondWithError
     *
     * @param mixed $message
     * @return void
     */
    public function respondWithError($message)
    {
        return $this->respond([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode(),
            ],
        ]);
    }
}
